﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HTERM
{
    public class PortSeriel
    {
        private SerialPort serialPort = null;
        private bool ouvert = false;
        private HTERM parent = null;

        public PortSeriel()
        {
            this.serialPort = new SerialPort();
        }

        public PortSeriel(HTERM fenetre)
        {
            parent = fenetre;
            this.serialPort = new SerialPort();
        }

        public bool Ouvert
        {
            get
            {
                return ouvert;
            }
        }

        public bool Ouvrir(string nomPort = "COM1", int baudRate = 9600)
        {
            if (ouvert == false)
            {
                try
                {
                    this.serialPort.PortName = nomPort;
                    this.serialPort.BaudRate = baudRate;
                    this.serialPort.DataBits = 8;
                    this.serialPort.Parity = Parity.None;
                    this.serialPort.StopBits = StopBits.One;
                    this.serialPort.Handshake = Handshake.None;

                    this.serialPort.Open();

                    this.serialPort.DiscardInBuffer();

                    this.ouvert = true;
                    return true;
                }
                catch (Exception erreur)
                {
                    parent.cboListePorts.Enabled = true;
                    MessageBox.Show("Problème à l'ouverture du port " + nomPort + " à la vitesse de " + baudRate + "   " + erreur.Message, "ERREUR !!");
                }
            }

            return false;
        }

        public bool Fermer()
        {
            if (ouvert == false) return false;

            else
            {
                try
                {
                    ouvert = false;
                    this.serialPort.Close();
                    return true;
                }
                catch (IOException erreur)
                {
                    MessageBox.Show("Problème à la fermeture du port " + erreur.Message);
                }
                return false;
            }
        }

        byte[] tampon = new byte[1];

        public bool EcrireOctet(byte octet)
        {
            if (ouvert == false) return false;

            tampon[0] = octet;
            this.serialPort.Write(tampon, 0, 1);

            return true;
        }

        public bool EcrireLigne(string tampon)
        {
            tampon = tampon + " ";
            
            if (ouvert == false) return false;

            this.serialPort.WriteLine(tampon);

            return true;
        }

        public int DonneesALire()
        {
            if (ouvert == false) return 0;

            return this.serialPort.BytesToRead;           
        }

        public bool LireOctet(ref byte octet)
        {
            if (ouvert == false) return false;

            octet = (byte)this.serialPort.ReadByte();

            return true;
        }
    }
}
