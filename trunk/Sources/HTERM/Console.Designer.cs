﻿namespace HTERM
{
    partial class HTERM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HTERM));
            this.btnOuvrirPort = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.btnFermerPort = new System.Windows.Forms.Button();
            this.cboListePorts = new System.Windows.Forms.ComboBox();
            this.txtEnvoi = new System.Windows.Forms.RichTextBox();
            this.txtRecep = new System.Windows.Forms.RichTextBox();
            this.lblEnvoi = new System.Windows.Forms.Label();
            this.lblRecep = new System.Windows.Forms.Label();
            this.lblPort = new System.Windows.Forms.Label();
            this.Minuterie = new System.Windows.Forms.Timer(this.components);
            this.btnErase = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOuvrirPort
            // 
            this.btnOuvrirPort.AutoSize = true;
            this.btnOuvrirPort.Enabled = false;
            this.btnOuvrirPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOuvrirPort.Location = new System.Drawing.Point(16, 17);
            this.btnOuvrirPort.Name = "btnOuvrirPort";
            this.btnOuvrirPort.Size = new System.Drawing.Size(120, 35);
            this.btnOuvrirPort.TabIndex = 0;
            this.btnOuvrirPort.Text = "Ouvrir Port";
            this.btnOuvrirPort.UseVisualStyleBackColor = true;
            this.btnOuvrirPort.Click += new System.EventHandler(this.btnOuvrirPort_Click);
            // 
            // btnFermerPort
            // 
            this.btnFermerPort.Enabled = false;
            this.btnFermerPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFermerPort.Location = new System.Drawing.Point(142, 17);
            this.btnFermerPort.Name = "btnFermerPort";
            this.btnFermerPort.Size = new System.Drawing.Size(120, 35);
            this.btnFermerPort.TabIndex = 1;
            this.btnFermerPort.Text = "Fermer port";
            this.btnFermerPort.UseVisualStyleBackColor = true;
            this.btnFermerPort.Click += new System.EventHandler(this.btnFermerPort_Click);
            // 
            // cboListePorts
            // 
            this.cboListePorts.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboListePorts.FormattingEnabled = true;
            this.cboListePorts.Location = new System.Drawing.Point(496, 17);
            this.cboListePorts.Name = "cboListePorts";
            this.cboListePorts.Size = new System.Drawing.Size(121, 21);
            this.cboListePorts.TabIndex = 2;
            this.cboListePorts.SelectedIndexChanged += new System.EventHandler(this.cboListePorts_SelectedIndexChanged);
            this.cboListePorts.Click += new System.EventHandler(this.cboListePorts_Click);
            // 
            // txtEnvoi
            // 
            this.txtEnvoi.Enabled = false;
            this.txtEnvoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEnvoi.Location = new System.Drawing.Point(22, 95);
            this.txtEnvoi.Name = "txtEnvoi";
            this.txtEnvoi.Size = new System.Drawing.Size(280, 307);
            this.txtEnvoi.TabIndex = 3;
            this.txtEnvoi.Text = "";
            this.txtEnvoi.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEnvoi_KeyDown);
            this.txtEnvoi.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtEnvoi_KeyUp);
            // 
            // txtRecep
            // 
            this.txtRecep.Enabled = false;
            this.txtRecep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRecep.Location = new System.Drawing.Point(330, 95);
            this.txtRecep.Name = "txtRecep";
            this.txtRecep.Size = new System.Drawing.Size(280, 307);
            this.txtRecep.TabIndex = 4;
            this.txtRecep.Text = "";
            // 
            // lblEnvoi
            // 
            this.lblEnvoi.AutoSize = true;
            this.lblEnvoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lblEnvoi.Location = new System.Drawing.Point(18, 63);
            this.lblEnvoi.Name = "lblEnvoi";
            this.lblEnvoi.Size = new System.Drawing.Size(43, 17);
            this.lblEnvoi.TabIndex = 5;
            this.lblEnvoi.Text = "Envoi";
            // 
            // lblRecep
            // 
            this.lblRecep.AutoSize = true;
            this.lblRecep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecep.Location = new System.Drawing.Point(326, 65);
            this.lblRecep.Name = "lblRecep";
            this.lblRecep.Size = new System.Drawing.Size(70, 16);
            this.lblRecep.TabIndex = 6;
            this.lblRecep.Text = "Réception";
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPort.Location = new System.Drawing.Point(442, 25);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(32, 13);
            this.lblPort.TabIndex = 7;
            this.lblPort.Text = "Port :";
            // 
            // Minuterie
            // 
            this.Minuterie.Tick += new System.EventHandler(this.Minuterie_Tick);
            // 
            // btnErase
            // 
            this.btnErase.Location = new System.Drawing.Point(500, 414);
            this.btnErase.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnErase.Name = "btnErase";
            this.btnErase.Size = new System.Drawing.Size(112, 35);
            this.btnErase.TabIndex = 8;
            this.btnErase.Text = "Effacer";
            this.btnErase.UseVisualStyleBackColor = true;
            this.btnErase.Click += new System.EventHandler(this.btnErase_Click);
            // 
            // HTERM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 466);
            this.Controls.Add(this.btnErase);
            this.Controls.Add(this.lblPort);
            this.Controls.Add(this.lblRecep);
            this.Controls.Add(this.lblEnvoi);
            this.Controls.Add(this.txtRecep);
            this.Controls.Add(this.txtEnvoi);
            this.Controls.Add(this.cboListePorts);
            this.Controls.Add(this.btnFermerPort);
            this.Controls.Add(this.btnOuvrirPort);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(644, 505);
            this.MinimumSize = new System.Drawing.Size(644, 505);
            this.Name = "HTERM";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HTERM";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.HTERM_FormClosed);
            this.Load += new System.EventHandler(this.HTERM_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOuvrirPort;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Button btnFermerPort;
        private System.Windows.Forms.RichTextBox txtEnvoi;
        private System.Windows.Forms.RichTextBox txtRecep;
        private System.Windows.Forms.Label lblEnvoi;
        private System.Windows.Forms.Label lblRecep;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.Timer Minuterie;
        private System.Windows.Forms.Button btnErase;
        public System.Windows.Forms.ComboBox cboListePorts;
    }
}

