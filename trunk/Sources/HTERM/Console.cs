﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HTERM
{
    public partial class HTERM : Form
    {
        public HTERM()
        {
            InitializeComponent();
        }

        #region Forme
        private void HTERM_Load(object sender, EventArgs e)
        {
            ps = new PortSeriel(this);
        }

        private void HTERM_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (ps.Ouvert) ps.Fermer();
        }
        #endregion

        #region Port COM

            #region Minuteries
        private void Minuterie_Tick(object sender, EventArgs e)
        {
            try
            {
                while (ps.DonneesALire() > 0)
                {
                    byte octet = 0;
                    if (ps.LireOctet(ref octet))
                    {
                        if (octet >= 32)
                        {
                            chaine.Append((char)octet);
                            if (chaine.Length > 450) chaine.Remove(0, 30);
                            txtRecep.Text = chaine.ToString();
                        }
                    }
                }
            }
            catch
            {
                Minuterie.Enabled = false;
                txtEnvoi.Enabled = false;
                btnOuvrirPort.Enabled = true;
                btnFermerPort.Enabled = false;
                cboListePorts.Enabled = true;
                ps.Fermer();
                MessageBox.Show("Le cable est débranché... ");
            }

        }
        #endregion

            #region Boutons
        private void btnOuvrirPort_Click(object sender, EventArgs e)
        {
            if (ps.Ouvrir(cboListePorts.Text, 9600))
            {
                btnOuvrirPort.Enabled = false;
                btnFermerPort.Enabled = true;
                Minuterie.Enabled = true;
                txtEnvoi.Enabled = true;
            }
        }

        private void btnFermerPort_Click(object sender, EventArgs e)
        {
            if (ps.Fermer())
            {
                btnOuvrirPort.Enabled = true;
                btnFermerPort.Enabled = false;
                cboListePorts.Enabled = true;
                Minuterie.Enabled = false;
            }            
        }

        private void cboListePorts_Click(object sender, EventArgs e)
        {
            cboListePorts.Items.Clear();
            String[] Ports = System.IO.Ports.SerialPort.GetPortNames();
            cboListePorts.Items.AddRange(Ports);
        }

        private void btnErase_Click(object sender, EventArgs e)
        {
            txtRecep.Text = "";
            chaine.Clear();
        }

        private void cboListePorts_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnOuvrirPort.Enabled = true;
        }
        #endregion

            #region Autres
        private PortSeriel ps = null;

        private StringBuilder chaine = new StringBuilder(400);
        #endregion

            #region Clavier
        private void txtEnvoi_KeyDown(object sender, KeyEventArgs e)
        {
            // Pour transmettre octet par octet
            //ps.EcrireOctet((byte)(e.KeyCode - '0'));

            if (e.KeyCode == Keys.Return)
            {
                ps.EcrireLigne(txtEnvoi.Text);
            }
        }

        private void txtEnvoi_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                txtEnvoi.Text = "";
            }
        }
        #endregion
        
        #endregion

    }
}
